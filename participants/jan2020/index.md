---
layout: people
---
### Held at SACEMA, Stellenbosch from 20-31&nbsp;January 2020.

![2020 Participants](participants.jpg)

***

{% assign people = site.data.jan2020 %}{% if people.size > 0%}{% assign people = people | sort %}{% for p in people %}{% assign part = p[1] %}{% if part.status == 'complete' %}{% include faculty.md profile=part %}
{% endif %}{% endfor %}{% else %}No participants!{% endif %}
