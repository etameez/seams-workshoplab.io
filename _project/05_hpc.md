---
slug: hpc
title: High Performance Computing
---
## Main Tasks
* Take a look at the current state of your code and identify parallelizable parts and non-parallelizable parts
* If your code is completely serial, think it can be re-written so that it or part of it is parallelizable
* Think about how to organize inputs, sections of code, and outputs in order to run your code in a parallel platform, **sketch this on paper**
* If you are based in South Africa find out if there is someone with a PI account on CHPC in your organization
* If you are from elsewhere find out if there is HPC facility in your country or region that you can access.
* Find out other HPC (Cloud Computing) services that are freely or commercially available to you

## Optional Tasks
- Explore HPC software like 
    - tensorflow, check out [First Steps with TensorFlow](https://colab.research.google.com/notebooks/mlcc/first_steps_with_tensor_flow.ipynb)
    - hadoop, 
    - mapreduce <!--(can find an online tutorial? re-purpose a bit for us?)-->
- Explore other HPC services providers like 
    - Google Colab
    - EC2 
    - Azure
    - NVIDIA
    - AWS 
    - etc <!--(can find an online tutorial? re-purpose a bit for us?)-->

<!--
During this time, we will identify capabilities you will need to provide in your
project and write tests for your *future* code.

Emphasis on *future*: write the tests for what will be accomplished first, then
the code.

First, write down some tests on paper.  You should be able to use your diagram
from [yesterday](project/organizing-inner/) to identify the parts that you are
concerned about, and the relationships or interactions between them you need to
consider.

Once, you have written down some tests on paper, translate an easy one (and one
that you think you know how to satisfy) to code.  If you're having trouble,
consult your project advisor for assistance.

Once you have a your simplest test down, try to satisfy it by writing project
code.

Then: Repeat!

As always, you should keep track of your progress with version control.  Your basic cycle should be:

 1. write a test that *fails*
 2. `git add` + `git commit` + `git push` test
 3. write code that satisfies test
 4. `git add` + `git commit` + `git push` code
 5. return to start
-->