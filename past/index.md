---
layout: default
permalink: /past/
---

## 2018 Workshop Participant Comments

> The SEAMS workshop condensed what could have easily been two months of code hacking into a productive and high intensity 2 week programme. The balance of practical to theoretical activities was great, the facilitators know their subject areas deeply and the delivery of my research outputs was accelerated thanks to the training.

> The amount of resources, knowledge and information provided during the workshop is priceless. It helped me move many steps further to the success of my project in 2 weeks than I would have spent in 2 months of office work. Thank you.

> Highly interactive workshop, addressing a huge knowledge and skills gap in data science and software engineering. I felt very fortunate to be able to participate in this workshop. It was a primer, a consolidator, an inspiration for many aspects and areas in my current and future work.

> [W]ith no formal background in computer programming, I’ve oftentimes suspected that my workflow has been less efficient and more time-consuming that it needed to be. Sharing work with colleagues was also oftentimes challenging. The workshop provided me with some excellent, basic best-practices for working on collaborative projects that involve programming and software development. I particularly appreciated the sessions on workspace organization and tools and reuse and reusability. I fully expect to use the skills and knowledge that I acquired during the workshop to improve the quality and efficiency of my professional work on future projects.

> This workshop tops the list of the workshops that I have attended. It is very student centered and you do get value for the time spent on it. The course offers a skill that is not exhaustively taught anywhere, but people always have to learn the hard way on good practices and the course touches this very well. I would recommend the course to other people interested in software development and not forgetting that the faculty members are knowledgeable and very attentive to their students and always give valuable feedback and can troubleshoot the problem with you.

> The workshop provides a platform for participants from different programming level backgrounds to all learn a new skill. Prior to the workshop I thought my coding/organisational/workflow skills were not too bad. I can now confidently say, at the end of the workshop, that is was actually embarrassingly bad. All the concepts taught throughout the workshop were familiar in that I heard about them but never quite understood the "how" to implement them. In addition, the implementation on a project that is relevant to your own work was extremely valuable. I learnt so much, not only from the lectures, practicals and faculty, but also from the participants.  Assistance and advice on my project was available throughout the 2 weeks. The faculty were available throughout the workshop, even during "break times", to chat, give advice and ideas, discuss progress and advise on the way forward.  I would very highly recommend the workshop to anyone working on a project, irrespective of the progress/stage of the project. I sincerely thank the faculty and organizers! Awesome job!!!
